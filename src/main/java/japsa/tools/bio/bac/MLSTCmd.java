/*****************************************************************************
 * Copyright (c) Minh Duc Cao, Monash Uni & UQ, All rights reserved.         *
 *                                                                           *
 * Redistribution and use in source and binary forms, with or without        *
 * modification, are permitted provided that the following conditions        *
 * are met:                                                                  * 
 *                                                                           *
 * 1. Redistributions of source code must retain the above copyright notice, *
 *    this list of conditions and the following disclaimer.                  *
 * 2. Redistributions in binary form must reproduce the above copyright      *
 *    notice, this list of conditions and the following disclaimer in the    *
 *    documentation and/or other materials provided with the distribution.   *
 * 3. Neither the names of the institutions nor the names of the contributors*
 *    may be used to endorse or promote products derived from this software  *
 *    without specific prior written permission.                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS   *
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, *
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR    *
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR         *
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,     *
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,       *
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR        *
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF    *
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING      *
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS        *
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.              *
 ****************************************************************************/

/*                           Revision History                                
 * 28/05/2014 - Minh Duc Cao: Created                                        
 ****************************************************************************/

package japsa.tools.bio.bac;


import java.io.IOException;
import java.util.ArrayList;

import japsa.bio.bac.MLSTyping;
import japsa.bio.bac.MLSTyping.MLSType;
import japsa.seq.Alphabet;
import japsa.seq.FastaReader;
import japsa.seq.Sequence;
import japsa.util.CommandLine;
import japsa.util.deploy.Deployable;

/**
 * @author minhduc
 *
 */
@Deployable(
	scriptName = "jsa.bac.mlst", 
	scriptDesc = "Multi-locus strain typing"
	)
public class MLSTCmd extends CommandLine{
	//CommandLine cmdLine;
	public MLSTCmd(){
		super();
		Deployable annotation = getClass().getAnnotation(Deployable.class);		
		setUsage(annotation.scriptName() + " [options]");
		setDesc(annotation.scriptDesc());

		addString("input", null, "Name of the genome file",true);
		addString("mlstScheme", null, "Folder contianing the allele files",true);
		addInt("top", 0, "If > 0, will provide top closest profile");

		addStdHelp();
	}

	public static void main(String [] args) throws IOException, InterruptedException{
		MLSTCmd cmdLine = new MLSTCmd ();
		args = cmdLine.stdParseLine(args);

		String input = cmdLine.getStringVal("input");
		String mlstDir = cmdLine.getStringVal("mlstScheme");
		int top = cmdLine.getIntVal("top");

		//String blastn = cmdLine.getStringVal("blastn");		
		ArrayList<Sequence> seqs = FastaReader.readAll(input, Alphabet.DNA());
		if (top <= 0)
			System.out.println(MLSTyping.bestMlst(seqs, mlstDir));
		else{
			MLSTyping t = MLSTyping.topMlst(seqs, mlstDir);
			for (int i = 0; i < 10; i++){
				MLSType p = t.getProfiles().get(i);
				System.out.println(p.getST() + " " + p.getScore());
			}

		}

	}
}
